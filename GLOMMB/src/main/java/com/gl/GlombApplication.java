package com.gl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.gl.model.Book;
import com.gl.model.Page;
import com.gl.repository.BookRepository;
import com.gl.repository.PageRepository;

@SpringBootApplication
public class GlombApplication implements CommandLineRunner {
	@Autowired
	BookRepository bookRepository;
	@Autowired
    PageRepository pageRepository;

	public static void main(String[] args) {
		SpringApplication.run(GlombApplication.class, args);
	}

	 @Override
	    public void run(String... args) {
		// create a new book
         Book book = new Book("Java 101", "John Doe", "123456");

         // save the book
         bookRepository.save(book);

         // create and save new pages
         pageRepository.save(new Page(1, "Introduction contents", "Introduction", book));
         pageRepository.save(new Page(65, "Java 8 contents", "Java 8", book));
         pageRepository.save(new Page(95, "Concurrency contents", "Concurrency", book));
     }
}
